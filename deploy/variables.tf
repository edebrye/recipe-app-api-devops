variable "prefix" {
  default     = "raad"
  description = "prefix for resources - recipe app api devops project"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "edebrye@ippon.fr"
}

variable "db_username" {
  description = "Username for PostgreSQL RDS instance"
}

variable "db_password" {
  description = "Password for PostgreSQL RDS instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "721665305066.dkr.ecr.us-west-1.amazonaws.com/edebrye-udemy-recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "721665305066.dkr.ecr.us-west-1.amazonaws.com/edebrye-terraform-learn:latest"

}

variable "django_secret_key" {
  description = "Secret Key for Django app"
}