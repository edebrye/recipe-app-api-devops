terraform {
  backend "s3" {
    bucket         = "edebrye-udemy-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-west-1"
    encrypt        = true
    dynamodb_table = "edebrye-udemy-recipe-app-api-devops-tfstate-lock"
  }
}

provider "aws" {
  region  = "us-west-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}